/**
 * Created by Lite on 19/03/2015.
 */
(function () {
    "use strict";
    var mainApp = angular
        .module("mainApp",
        ['profileModule', 'timetableModule', 'teachersModule',
         'noticesModule', 'examResultModule', 'teacherRemarksModule', 'homeworkModule', 'busRouteModule',
         'chatRoomModule', 'ui.router']);

    mainApp.config([
        "$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise("/");
            $stateProvider
                // Home pages
                .state("profile",{
                    url: "/",
                    templateUrl: "app/profile/profileView.html",
                    controller: "ProfileCtrl"
                })
                .state("timetable",{
                    url: "/timetable",
                    templateUrl: "app/timetable/timetableView.html",
                    controller: "TimetableCtrl"
                })
                .state("teachers",{
                    url: "/teachers",
                    templateUrl: "app/teachers/teachersView.html",
                    controller: "TeachersCtrl"
                })
                .state("notices",{
                    url: "/notices",
                    templateUrl: "app/notices/noticesView.html",
                    controller: "NoticesCtrl"
                })
                .state("homework",{
                    url: "/homework",
                    templateUrl: "app/homework/homeworkView.html",
                    controller: "HomeworkCtrl"
                })
                .state("examResult",{
                    url: "/examResult",
                    templateUrl: "app/examResult/examResultView.html",
                    controller: "ExamResultCtrl"
                })
                .state("teacherRemarks",{
                    url: "/teacherRemarks",
                    templateUrl: "app/teacherRemarks/teacherRemarksView.html",
                    controller: "TeacherRemarksCtrl"
                })
                .state("chatRoom",{
                    url: "/chatRoom",
                    templateUrl: "app/chatRoom/chatRoomView.html",
                    controller: "ChatRoomCtrl"
                })
                .state("busRoute",{
                    url: "/busRoute",
                    templateUrl: "app/busRoute/busRouteView.html",
                    controller: "BusRouteCtrl"
                })
        }
    ])
}());